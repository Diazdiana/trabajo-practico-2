package tp2;

import static org.junit.Assert.*;
import java.util.Set;

import org.junit.Test;

public class BFSTest {
	@Test (expected=IllegalArgumentException.class)
	public void grafoNuloTest() {
		BFS.esConexo(null);
	}
	
	@Test
	public void grafoVacioTest() {
		Grafo g = new Grafo(0);
		assertTrue(BFS.esConexo(g));
	}
	
	@Test
	public void grafoNoConexoTest() {
		Grafo g = inicializarGrafo();
		assertFalse(BFS.esConexo(g));
	}
	
	@Test
	public void grafoConexoTest() {
		Grafo g = inicializarGrafo();
		g.agregarArista(3, 4, 1);
		assertTrue(BFS.esConexo(g));
	}
	
	@Test
	public void alcanzablesTest() {
		Grafo g = inicializarGrafo();
		Set<Integer> alcanzables = BFS.alcanzables(g,0);
		Assert.iguales(new int[] {0,1,2,3}, alcanzables);
	}

	private Grafo inicializarGrafo() {
		Grafo grafo = new Grafo(5);
		grafo.agregarArista(0, 1, 1);
		grafo.agregarArista(0, 2, 2);
		grafo.agregarArista(2, 3, 3);
		return grafo;
	}

	
}
