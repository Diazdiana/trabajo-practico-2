package tp2;

import static org.junit.Assert.*;

import javax.swing.DefaultListModel;

import org.junit.Before;
import org.junit.Test;



public class EspiasTest {
	 Espias grupoespias;

	@Before 
	public void inicializar() {
	this.grupoespias= new Espias(); 
	grupoespias.agregarEspia("fede");
	grupoespias.agregarEspia("diana");
	grupoespias.agregarEspia("nelson");
		
		
		
		
	}
	
	



	@Test
	public void testAgregarEspia() { //agrega un espia al grupo de espias
//		Espias grupoEspias= new Espias();		
//		grupoespias.agregarEspia("fede");
		assertEquals(3, grupoespias.getCantEspias());
		assertTrue(grupoespias.existeEspia("fede"));


		
	}

	@Test
	public void testGetCantEspias() { //devuelve la cantidad de espias
//		Espias grupoEspias= new Espias();		
//		grupoespias.agregarEspia("fede");
		assertEquals(3, grupoespias.CantEspias);
	}

	@Test
	public void testAgregarListadeEspias() { //ingresa la lista default a la lista jlist para ser visible
//		Espias grupoEspias= new Espias();
		DefaultListModel<String> lista = new DefaultListModel<String> () ;
		lista.add(0, "fede");
		lista.add(1, "diana");
		lista.add(2, "nelson");
		grupoespias.agregarListadeEspias(lista);
		assertEquals(6, grupoespias.getCantEspias());
	}

	@Test
	public void testNumEspia() { // // devuelve el numero de espia
//		Espias grupoEspias= new Espias();
//		grupoespias.agregarEspia("fede");
//		grupoespias.agregarEspia("diana");
//		grupoespias.agregarEspia("nelson");
		assertTrue(grupoespias.numEspia("nelson")==2);
		

		
	}

	@Test
	public void testNombreEspia() { //devuelve el nombre del espia a partir del numero
//		Espias grupoEspias= new Espias();
//		grupoespias.agregarEspia("fede");
//		grupoespias.agregarEspia("diana");
//		grupoespias.agregarEspia("nelson");
		assertEquals("diana", grupoespias.nombreEspia(1));

	}
	
	@Test
	public void testexisteEspia() { // devuelve verdadero si existe el espia en la lista
//		Espias grupoEspias= new Espias();
//		grupoespias.agregarEspia("fede");
//		grupoespias.agregarEspia("diana");
//		grupoespias.agregarEspia("nelson");
		assertTrue(grupoespias.existeEspia("nelson"));
		assertTrue(!grupoespias.existeEspia("espia"));
		
	}

}

