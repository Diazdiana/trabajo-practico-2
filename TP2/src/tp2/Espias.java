package tp2;
import java.io.Serializable;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.util.HashMap;
import java.util.Map;
import javax.swing.DefaultListModel;


public class Espias implements Serializable{
	Map <String,Integer> Espias;
	int CantEspias;
	private static final long serialVersionUID = 1L;

	public Espias () {
		this.Espias=new HashMap<String,Integer> ();
		this.CantEspias=0;
	}
	
	public void agregarEspia (String nombre) {
		
		this.Espias.put(nombre,getCantEspias());
		this.CantEspias++;
		
	}
		
	public int getCantEspias() {
		return CantEspias;
	}
	
	public void agregarListadeEspias (DefaultListModel<String> lista) {
		for (int i = 0 ; i<lista.getSize();i++) {
			agregarEspia(lista.get(i));
		}
	}
	
	public Integer numEspia (String nombre) {
		if (this.Espias.containsKey(nombre)) {
			return this.Espias.get(nombre);	
		}
		else {
			return null ;
		}
	}
	
	public String nombreEspia (Integer numero) {
		for (Map.Entry<String,Integer> entry : this.Espias.entrySet()) {
		    if (entry.getValue()== numero) {
		    	return entry.getKey();
		    }
		}
		return null;
	}
	public boolean existeEspia (String nombre) {
		if (Espias.containsKey(nombre)) {
			return true;
		}
		else {
			return false;
		}
	}
	
	
}

