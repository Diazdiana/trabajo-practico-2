package tp2;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class GrafoTest {
	Grafo grafo;

	
	@Before
	public void setUp() {
		this.grafo = new Grafo (3);
		grafo.agregarArista(0, 1, 2);
		grafo.agregarArista(0, 2, 3);
		grafo.agregarArista(1, 2, 5);
		
	}

	

	@Test
	public void testAgregarArista() { // agrega el peso de la arista
		Grafo test = new Grafo (2);
		test.agregarArista(0, 1, 2);
		assertTrue(test.existeArista(0, 1));
	}

	@Test
	public void testPesoDeArista() {// devuelve el peso de la arista
		assertTrue(grafo.pesoDeArista(0, 1)==2);
	}

	@Test
	public void testEliminarArista() { // elimina una arista de un grafo
		Grafo test = new Grafo (2);
		test.agregarArista(0, 1, 2);
		test.eliminarArista(0, 1);
		assertFalse(test.existeArista(0, 1));
	}

	@Test
	public void testExisteArista() { // devuelve verdadero si existe arista
		assertTrue(grafo.existeArista(0, 1));
		
	}

	@Test
	public void testTamano() { //devuelve la cantidad de verticas
		assertTrue(grafo.tamano()==3);
	}

	@Test
	public void testVecinos() { //devuelve los vecinos de un vertice
		Set<Integer> ret = new HashSet<Integer>();
		ret=grafo.vecinos(0);
		assertTrue(ret.size()==2);
		assertTrue(ret.contains(1)&& ret.contains(2) && !ret.contains(3));
	}

	@Test
	public void testCaminoMinimo() { // devuelve los pares de vertices que conforman el camino minimo
		ArrayList<Integer> caminoMinimo = new ArrayList<Integer> ();
		ArrayList<Integer> caminoMinimo2 = new ArrayList<Integer> ();
		caminoMinimo=grafo.caminoMinimo(0);
		caminoMinimo2.add(0);
		caminoMinimo2.add(1);
		caminoMinimo2.add(0);
		caminoMinimo2.add(2);
		
		assertTrue(caminoMinimo.size()==4);
		assertTrue(caminoMinimo.equals(caminoMinimo2));
	}

}
