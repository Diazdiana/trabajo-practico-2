package vista;

import java.awt.EventQueue;

import javax.swing.JFrame;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.table.DefaultTableModel;

import tp2.Espias;
import tp2.Grafo;
import tp2.BFS;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;
import javax.swing.JTextArea;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.JTable;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.Font;
import java.awt.SystemColor;
import java.awt.Color;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;

public class form {

	private JFrame frame;
	private JTextField campoIngresarEspia;
	private String NombreEspia;
	private static Espias GrupoEspias;
	private static Grafo MapaEspias;
	private JTextField txtAgregueElPeso;
	private JTable tablaMatrizEspias;
	private JTable tableResultado;
	private ArrayList <Integer> caminominimo;
	private BFS recorrerGrafo;


	/**
	 * Launch the application.
	 */

	public static void agregaraComboBox (JComboBox<String> combi ,DefaultListModel<String> espias ) {
		for (int i = 0 ; i<espias.getSize();i++) {
			combi.addItem(espias.get(i));
		}
	}

	public static void main(String[] args) {
		
		GrupoEspias=new Espias ();


		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					form window = new form();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public form() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.getContentPane().setBackground(new Color(248, 248, 255));
		frame.setBounds(100, 100, 996, 527);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		DefaultListModel<String> lista = new DefaultListModel<String>();
		DefaultTableModel MatrizEspias = new DefaultTableModel();
		DefaultTableModel Matrizresultado = new DefaultTableModel();
		MatrizEspias.addColumn("Espia1");
		MatrizEspias.addColumn("Espia2");
		MatrizEspias.addColumn("Peso");
		Matrizresultado.addColumn("Espia1");
		Matrizresultado.addColumn("Espia2");
		Matrizresultado.addColumn("Peso");
		JButton botonCaminoMinimo = new JButton("Obtener Camino Minimo");
		botonCaminoMinimo.setFont(new Font("Tahoma", Font.PLAIN, 13));
		botonCaminoMinimo.setBackground(new Color(245, 245, 245));
		JScrollPane scrollPaneresultado = new JScrollPane();
		JButton Boton_CrearRelacion = new JButton("Crear Relacion");
		Boton_CrearRelacion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		Boton_CrearRelacion.setBackground(new Color(245, 245, 245));
		scrollPaneresultado.setBounds(636, 69, 223, 244);
		JButton btnBorrarRelacion = new JButton("Borrar Relacion ");
		btnBorrarRelacion.setFont(new Font("Tahoma", Font.PLAIN, 13));
		btnBorrarRelacion.setBackground(new Color(245, 245, 245));
		frame.getContentPane().add(scrollPaneresultado);

		tableResultado = new JTable();
		scrollPaneresultado.setViewportView(tableResultado);
		tableResultado.setModel(Matrizresultado);



		JScrollPane MatrizRelaciones = new JScrollPane();
		MatrizRelaciones.setBounds(361, 272, 195, 184);
		frame.getContentPane().add(MatrizRelaciones);

		tablaMatrizEspias = new JTable();
		tablaMatrizEspias.setFont(new Font("Tahoma", Font.PLAIN, 14));
		tablaMatrizEspias.setBackground(Color.WHITE);
		MatrizRelaciones.setViewportView(tablaMatrizEspias);
		frame.getContentPane().setLayout(null);
		tablaMatrizEspias.setModel(MatrizEspias);



		JComboBox<String> comboBox_Espias1 = new JComboBox<String>();
		
		JComboBox<String> comboBox_Espias2 = new JComboBox<String>();
		
		
		
		comboBox_Espias1.setBounds(388, 66, 145, 22);
		frame.getContentPane().add(comboBox_Espias1);	

		
		comboBox_Espias2.setBounds(388, 126, 145, 20);
		frame.getContentPane().add(comboBox_Espias2);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 215, 292, 146);
		frame.getContentPane().add(scrollPane);

		JList<String> ListaEspias = new JList<String>();
		scrollPane.setViewportView(ListaEspias);

		JLabel LabelIngresarEspia = new JLabel("INGRESE NOMBRE DEL ESPIA");
		LabelIngresarEspia.setForeground(new Color(255, 255, 255));
		LabelIngresarEspia.setBackground(Color.CYAN);
		LabelIngresarEspia.setFont(new Font("Arial Black", Font.PLAIN, 14));
		LabelIngresarEspia.setBounds(45, 59, 236, 36);
		frame.getContentPane().add(LabelIngresarEspia);

		campoIngresarEspia = new JTextField();

		campoIngresarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAgregarEspia(lista, ListaEspias);
			}
		});
		campoIngresarEspia.setBounds(45, 104, 122, 20);
		frame.getContentPane().add(campoIngresarEspia);
		campoIngresarEspia.setColumns(2);
		JButton BorrarEspia = new JButton("Borrar Espia");
		BorrarEspia.setFont(new Font("Tahoma", Font.PLAIN, 13));
		BorrarEspia.setBackground(new Color(245, 245, 245));

		JButton BotonIngresarEspia = new JButton("Ingresar");
		BotonIngresarEspia.setBackground(new Color(245, 245, 245));
		BotonIngresarEspia.setFont(new Font("Tahoma", Font.PLAIN, 14));
		BotonIngresarEspia.setBounds(62, 148, 89, 23);
		BotonIngresarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAgregarEspia(lista, ListaEspias);
			}
		});
		frame.getContentPane().add(BotonIngresarEspia);
		
		JButton GuardarLista = new JButton("Guardar lista");
		GuardarLista.setFont(new Font("Tahoma", Font.PLAIN, 13));
		GuardarLista.setBackground(new Color(245, 245, 245));
		GuardarLista.setBounds(169, 372, 133, 29);
		GuardarLista.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (lista.getSize()>1) {
				GrupoEspias.agregarListadeEspias(lista);
				agregaraComboBox (comboBox_Espias1,lista);
				agregaraComboBox (comboBox_Espias2,lista);
				MapaEspias= new Grafo (lista.getSize());

				lista.removeAllElements();
				ListaEspias.setModel(lista);
				BotonIngresarEspia.setEnabled(false);
				campoIngresarEspia.setEnabled(false);
				BorrarEspia.setEnabled(false);
				GuardarLista.setEnabled(false);
				btnBorrarRelacion.setEnabled(true);
				Boton_CrearRelacion.setEnabled(true);
				txtAgregueElPeso.setEnabled(true);
				
				}
				else {
					JOptionPane.showMessageDialog(null,
							"Debe tener minimo 2 espias",
							"Error",
							JOptionPane.INFORMATION_MESSAGE);
				}
			}
		});
		frame.getContentPane().add(GuardarLista);
		
		
		
		BorrarEspia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (ListaEspias.getSelectedIndex()>=0) {
				lista.remove(ListaEspias.getSelectedIndex());
		
			}
			}
		});
		BorrarEspia.setBounds(20, 372, 122, 29);
		frame.getContentPane().add(BorrarEspia);




		txtAgregueElPeso = new JTextField();
		txtAgregueElPeso.setEnabled(false);
		txtAgregueElPeso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				agregarRelacion(MatrizEspias, comboBox_Espias1, comboBox_Espias2);
				
			}
		});
		txtAgregueElPeso.setToolTipText("");
		txtAgregueElPeso.setBounds(388, 186, 145, 20);
		frame.getContentPane().add(txtAgregueElPeso);
		txtAgregueElPeso.setColumns(10);

		
		Boton_CrearRelacion.setEnabled(false);
		Boton_CrearRelacion.setBounds(388, 230, 145, 23);
		Boton_CrearRelacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				
				agregarRelacion(MatrizEspias, comboBox_Espias1, comboBox_Espias2);
				botonCaminoMinimo.setEnabled(true);
				
				
				
			}
		}
				);
		frame.getContentPane().add(Boton_CrearRelacion);
		

		
		botonCaminoMinimo.setEnabled(false);
		botonCaminoMinimo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(recorrerGrafo.esConexo(MapaEspias)) {
				caminominimo=MapaEspias.caminoMinimo(0);
				for (int i =0 ; i<caminominimo.size()-1;i=i+2) {
					Matrizresultado.addRow(new String [] {GrupoEspias.nombreEspia(caminominimo.get(i)),GrupoEspias.nombreEspia(caminominimo.get(i+1)),MapaEspias.pesoDeArista(caminominimo.get(i), caminominimo.get(i+1)).toString()});

				}
				}
				else {
					JOptionPane.showMessageDialog(null,
							"El grafo debe ser conexo",
							"Error",
							JOptionPane.INFORMATION_MESSAGE);
				}

			}
		});
		botonCaminoMinimo.setBounds(646, 325, 200, 26);
		frame.getContentPane().add(botonCaminoMinimo);
		
		
		btnBorrarRelacion.setEnabled(false);
		btnBorrarRelacion.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (tablaMatrizEspias.getRowCount()!=0) {
				int numeroFila =tablaMatrizEspias.getSelectedRow();				
				String Espia1 = tablaMatrizEspias.getValueAt(numeroFila, 0).toString();
				String Espia2 = tablaMatrizEspias.getValueAt(numeroFila, 1).toString();
				MapaEspias.eliminarArista( GrupoEspias.numEspia(Espia1), GrupoEspias.numEspia(Espia2));
				MatrizEspias.removeRow(numeroFila);
				}
				else {
					JOptionPane.showMessageDialog(null,
							"No hay relaciones que borrar",
							"Error",
							JOptionPane.INFORMATION_MESSAGE);
				}

				
			}
		});
		btnBorrarRelacion.setBounds(383, 467, 150, 23);
		frame.getContentPane().add(btnBorrarRelacion);
		
		JButton btnNewButton = new JButton("SALIR");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		btnNewButton.setForeground(new Color(0, 0, 0));
		btnNewButton.setFont(new Font("Arial Black", Font.PLAIN, 20));
		btnNewButton.setBounds(726, 420, 133, 60);
		frame.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("Espia1");
		lblNewLabel.setForeground(new Color(255, 255, 255));
		lblNewLabel.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel.setBounds(388, 44, 89, 22);
		frame.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("Espia2");
		lblNewLabel_1.setForeground(new Color(255, 255, 255));
		lblNewLabel_1.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel_1.setBounds(388, 107, 58, 17);
		frame.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Peso de la arista ");
		lblNewLabel_2.setForeground(new Color(255, 255, 255));
		lblNewLabel_2.setFont(new Font("Arial Black", Font.PLAIN, 14));
		lblNewLabel_2.setBounds(388, 169, 145, 13);
		frame.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("New label");
		lblNewLabel_3.setIcon(new ImageIcon("C:\\Users\\USUARIO\\Desktop\\Im.jpeg"));
		lblNewLabel_3.setBounds(0, 0, 982, 490);
		frame.getContentPane().add(lblNewLabel_3);
		
		

	}
		
		
		public void btnAgregarEspia(DefaultListModel<String> lista, JList<String> ListaEspias) {
			
	
		if(campoIngresarEspia.getText().trim().isEmpty() ) {
			JOptionPane.showMessageDialog(null,
					"Debe ingresar un nombre para el espia",
					"Error",
					JOptionPane.INFORMATION_MESSAGE);
		}
		else {
		NombreEspia=campoIngresarEspia.getText();
		if (!lista.contains(NombreEspia)) {
			lista.addElement(NombreEspia);	// lista que se visualiza
			campoIngresarEspia.setText(null);
			campoIngresarEspia.requestFocus();
			ListaEspias.setModel(lista);
		
		}
		else {
			JOptionPane.showMessageDialog(null,
					"no puede agregarse 2 veces el mismo espia",
					"Error",
					JOptionPane.INFORMATION_MESSAGE);
		}
	}
}

	public void agregarRelacion(DefaultTableModel MatrizEspias, JComboBox<String> comboBox_Espias1,
			JComboBox<String> comboBox_Espias2) {
		if (txtAgregueElPeso.getText().isEmpty() || !txtAgregueElPeso.getText().chars().allMatch( Character::isDigit )) {
			JOptionPane.showMessageDialog(null,
					"Debe ingresar un numero entero para crear una relacion",
					"Error",
					JOptionPane.INFORMATION_MESSAGE);
		
		}
		
		
		else {
		if (!comboBox_Espias1.getSelectedItem().equals(comboBox_Espias2.getSelectedItem())) {	
			if (MapaEspias.existeArista(GrupoEspias.numEspia((String) comboBox_Espias1.getSelectedItem()), GrupoEspias.numEspia((String) comboBox_Espias2.getSelectedItem()))) {
				JOptionPane.showMessageDialog(null,
						"No se puede crear mas de una relacion entre los mismos espias",
						"Error",
						JOptionPane.INFORMATION_MESSAGE);
			}
			else {
			MapaEspias.agregarArista(GrupoEspias.numEspia((String) comboBox_Espias1.getSelectedItem()), GrupoEspias.numEspia((String) comboBox_Espias2.getSelectedItem()), Integer.parseInt(txtAgregueElPeso.getText()));
			MatrizEspias.addRow(new String [] {(String) comboBox_Espias1.getSelectedItem(),(String) comboBox_Espias2.getSelectedItem(),txtAgregueElPeso.getText()});
			tablaMatrizEspias.setModel(MatrizEspias);
			
			
			}
		}
		else {
			JOptionPane.showMessageDialog(null,
					"No se puede crear una relacion entre el mismo espia",
					"Error",
					JOptionPane.INFORMATION_MESSAGE);
		}
		
}
	}	
}
